FROM ubuntu:20.04
LABEL mantainer="sandro.caetano@spyderlabs.com"

# -----

USER root
ENV DEBIAN_FRONTEND noninteractive


# Setup and install base system software
RUN echo "locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8" | debconf-set-selections \
    && echo "locales locales/default_environment_locale select en_US.UTF-8" | debconf-set-selections \
    && apt-get update \
    && apt-get --yes --no-install-recommends install \
        locales tzdata ca-certificates sudo \
    && rm -rf /var/lib/apt/lists/*
ENV LANG en_US.UTF-8


# Install Python stack
RUN apt-get update \
    && apt-get --yes --no-install-recommends install \
        python3 python3-dev \
        python3-pip python3-venv python3-wheel python3-setuptools \
        build-essential cmake \
        graphviz libpq-dev\
        libssl-dev libffi-dev \
        postgresql postgresql-contrib \
    && rm -rf /var/lib/apt/lists/*


# Install Python modules
ADD ./requirements.txt /tmp/requirements.txt
RUN pip3 install --no-cache-dir -r /tmp/requirements.txt \
    && rm -rf ~/.cache/pip

RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 1

ADD . /app
WORKDIR /app


